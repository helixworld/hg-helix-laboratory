/**
* Service 接口层，用于封装业务逻辑，请在这声明要定义的业务接口方法
* @author Allen Wang
* @date 2022/7/27 17:03
*/

package com.helix.laboratory.openfeign.service;