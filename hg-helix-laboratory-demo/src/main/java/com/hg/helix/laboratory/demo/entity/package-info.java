/**
* Entity 包主要存放 Mybatis Entity 对象，用于配置 Java POJO 对象属性与数据库字段的对应关系。
* @author Allen Wang
* @date 2022/7/27 17:18
*/

package com.hg.helix.laboratory.demo.entity;