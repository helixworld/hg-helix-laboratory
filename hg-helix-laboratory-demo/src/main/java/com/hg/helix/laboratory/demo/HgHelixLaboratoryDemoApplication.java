package com.hg.helix.laboratory.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * All rights Reserved, Designed By www.hgplan.cn
 *
 * @author [Allen Wang]
 * @version V1.0 Copyright 2022 www.hgplan.cn Inc. All rights reserved.
 * @date 2022/7/27 15:02
 */
@SpringBootApplication
@ComponentScan("com.hg")
public class HgHelixLaboratoryDemoApplication {
  public static void main(String[] args) {
    SpringApplication.run(HgHelixLaboratoryDemoApplication.class);
  }
}