package com.hg.helix.laboratory.demo.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * All rights Reserved, Designed By www.hgplan.cn
 * Entity Basebean
 * @author [Allen Wang]
 * @version V1.0
 * Copyright 2022 www.hgplan.cn Inc. All rights reserved.
 * @date 2022/7/27 16:08
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseEntity {
  private String createdUser;
  private Date createdTime;
  private String modifiedUser;
  private Date modifiedTime;
}